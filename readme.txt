=== MJFreewaySync ===
Contributors: <a target="_blank" src="http://dev200.co.za/">Dev200</a>
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Sync management tool for MJFreeway stock

== Description ==

Sync management tool for MJFreeway stock

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
