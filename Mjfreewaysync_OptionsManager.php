<?php
/*
    "WordPress Plugin Template" Copyright (C) 2015 Michael Simpson  (email : michael.d.simpson@gmail.com)

    This file is part of WordPress Plugin Template for WordPress.

    WordPress Plugin Template is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordPress Plugin Template is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Contact Form to Database Extension.
    If not, see http://www.gnu.org/licenses/gpl-3.0.html
*/

class Mjfreewaysync_OptionsManager {
    
    public function Sync(){
        $this->updateOption($_POST['Branch'] . "_URL", $_POST['URL']);
        $this->updateOption($_POST['Branch'] . "_AliasURL", $_POST['AliasURL']);
        $this->updateOption($_POST['Branch'] . "_NID", $_POST['NID']);
        $this->updateOption($_POST['Branch'] . "_APIID", $_POST['APIID']);
        $this->updateOption($_POST['Branch'] . "_APIKey", $_POST['APIKey']);
    }

    public function getOptionNamePrefix() {
        return get_class($this) . '_';
    }


    /**
     * Define your options meta data here as an array, where each element in the array
     * @return array of key=>display-name and/or key=>array(display-name, choice1, choice2, ...)
     * key: an option name for the key (this name will be given a prefix when stored in
     * the database to ensure it does not conflict with other plugin options)
     * value: can be one of two things:
     *   (1) string display name for displaying the name of the option to the user on a web page
     *   (2) array where the first element is a display name (as above) and the rest of
     *       the elements are choices of values that the user can select
     * e.g.
     * array(
     *   'item' => 'Item:',             // key => display-name
     *   'rating' => array(             // key => array ( display-name, choice1, choice2, ...)
     *       'CanDoOperationX' => array('Can do Operation X', 'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber'),
     *       'Rating:', 'Excellent', 'Good', 'Fair', 'Poor')
     */
    public function getOptionMetaData() {
        return array();
    }

    /**
     * @return array of string name of options
     */
    public function getOptionNames() {
        return array_keys($this->getOptionMetaData());
    }

    /**
     * Override this method to initialize options to default values and save to the database with add_option
     * @return void
     */
    protected function initOptions() {
    }

    /**
     * Cleanup: remove all options from the DB
     * @return void
     */
    protected function deleteSavedOptions() {
        $optionMetaData = $this->getOptionMetaData();
        if (is_array($optionMetaData)) {
            foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                $prefixedOptionName = $this->prefix($aOptionKey); // how it is stored in DB
                delete_option($prefixedOptionName);
            }
        }
    }

    /**
     * @return string display name of the plugin to show as a name/title in HTML.
     * Just returns the class name. Override this method to return something more readable
     */
    public function getPluginDisplayName() {
        return get_class($this);
    }

    /**
     * Get the prefixed version input $name suitable for storing in WP options
     * Idempotent: if $optionName is already prefixed, it is not prefixed again, it is returned without change
     * @param  $name string option name to prefix. Defined in settings.php and set as keys of $this->optionMetaData
     * @return string
     */
    public function prefix($name) {
        $optionNamePrefix = $this->getOptionNamePrefix();
        if (strpos($name, $optionNamePrefix) === 0) { // 0 but not false
            return $name; // already prefixed
        }
        return $optionNamePrefix . $name;
    }

    /**
     * Remove the prefix from the input $name.
     * Idempotent: If no prefix found, just returns what was input.
     * @param  $name string
     * @return string $optionName without the prefix.
     */
    public function &unPrefix($name) {
        $optionNamePrefix = $this->getOptionNamePrefix();
        if (strpos($name, $optionNamePrefix) === 0) {
            return substr($name, strlen($optionNamePrefix));
        }
        return $name;
    }

    /**
     * A wrapper function delegating to WP get_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param $default string default value to return if the option is not set
     * @return string the value from delegated call to get_option(), or optional default value
     * if option is not set.
     */
    public function getOption($optionName, $default = null) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        $retVal = get_option($prefixedOptionName);
        if (!$retVal && $default) {
            $retVal = $default;
        }
        return $retVal;
    }

    /**
     * A wrapper function delegating to WP delete_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @return bool from delegated call to delete_option()
     */
    public function deleteOption($optionName) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return delete_option($prefixedOptionName);
    }

    /**
     * A wrapper function delegating to WP add_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param  $value mixed the new value
     * @return null from delegated call to delete_option()
     */
    public function addOption($optionName, $value) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return add_option($prefixedOptionName, $value);
    }

    /**
     * A wrapper function delegating to WP add_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param  $value mixed the new value
     * @return null from delegated call to delete_option()
     */
    public function updateOption($optionName, $value) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return update_option($prefixedOptionName, $value);
    }

    /**
     * A Role Option is an option defined in getOptionMetaData() as a choice of WP standard roles, e.g.
     * 'CanDoOperationX' => array('Can do Operation X', 'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber')
     * The idea is use an option to indicate what role level a user must minimally have in order to do some operation.
     * So if a Role Option 'CanDoOperationX' is set to 'Editor' then users which role 'Editor' or above should be
     * able to do Operation X.
     * Also see: canUserDoRoleOption()
     * @param  $optionName
     * @return string role name
     */
    public function getRoleOption($optionName) {
        $roleAllowed = $this->getOption($optionName);
        if (!$roleAllowed || $roleAllowed == '') {
            $roleAllowed = 'Administrator';
        }
        return $roleAllowed;
    }

    /**
     * Given a WP role name, return a WP capability which only that role and roles above it have
     * http://codex.wordpress.org/Roles_and_Capabilities
     * @param  $roleName
     * @return string a WP capability or '' if unknown input role
     */
    protected function roleToCapability($roleName) {
        switch ($roleName) {
            case 'Super Admin':
                return 'manage_options';
            case 'Administrator':
                return 'manage_options';
            case 'Editor':
                return 'publish_pages';
            case 'Author':
                return 'publish_posts';
            case 'Contributor':
                return 'edit_posts';
            case 'Subscriber':
                return 'read';
            case 'Anyone':
                return 'read';
        }
        return '';
    }

    /**
     * @param $roleName string a standard WP role name like 'Administrator'
     * @return bool
     */
    public function isUserRoleEqualOrBetterThan($roleName) {
        if ('Anyone' == $roleName) {
            return true;
        }
        $capability = $this->roleToCapability($roleName);
        return current_user_can($capability);
    }

    /**
     * @param  $optionName string name of a Role option (see comments in getRoleOption())
     * @return bool indicates if the user has adequate permissions
     */
    public function canUserDoRoleOption($optionName) {
        $roleAllowed = $this->getRoleOption($optionName);
        if ('Anyone' == $roleAllowed) {
            return true;
        }
        return $this->isUserRoleEqualOrBetterThan($roleAllowed);
    }

    /**
     * see: http://codex.wordpress.org/Creating_Options_Pages
     * @return void
     */
    public function createSettingsMenu() {
        $pluginName = $this->getPluginDisplayName();
        //create new top-level menu
        add_menu_page($pluginName . ' Plugin Settings',
                      $pluginName,
                      'administrator',
                      get_class($this),
                      array(&$this, 'settingsPage')
        /*,plugins_url('/images/icon.png', __FILE__)*/); // if you call 'plugins_url; be sure to "require_once" it

        //call register settings function
        add_action('admin_init', array(&$this, 'registerSettings'));
    }

    public function registerSettings() {
        $settingsGroup = get_class($this) . '-settings-group';
        $optionMetaData = $this->getOptionMetaData();
        foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
            register_setting($settingsGroup, $aOptionMeta);
        }
    }

    /**
     * Creates HTML for the Administration page to set options for this plugin.
     * Override this method to create a customized page.
     * @return void
     */
     
    public function settingsPage() {
        
        if (isset($_GET['DoCron'])){
            // $this->DeleteRecords();
            
            $this->getRecords($this->getOption('Phoenix_URL'), $this->getOption('Phoenix_APIID'), $this->getOption('Phoenix_APIKey'), 'phoenix');
            $this->getRecords($this->getOption('Tuscon_URL'), $this->getOption('Tuscon_APIID'), $this->getOption('Tuscon_APIKey'), 'tuscon');
            $this->getRecords($this->getOption('Sedona_URL'), $this->getOption('Sedona_APIID'), $this->getOption('Sedona_APIKey'), 'sedona');
            $this->getRecords($this->getOption('Copper Country_URL'), $this->getOption('Copper Country_APIID'), $this->getOption('Copper Country_APIKey'), 'copper-country');
        }

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'mjfreewaysync'));
        }

        $optionMetaData = $this->getOptionMetaData();

        if (isset($_POST["Sync"])){
            // $this->DeleteRecords();
            
            $this->getRecords($this->getOption('Phoenix_URL'), $this->getOption('Phoenix_APIID'), $this->getOption('Phoenix_APIKey'), 'phoenix');
            $this->getRecords($this->getOption('Tuscon_URL'), $this->getOption('Tuscon_APIID'), $this->getOption('Tuscon_APIKey'), 'tuscon');
            $this->getRecords($this->getOption('Sedona_URL'), $this->getOption('Sedona_APIID'), $this->getOption('Sedona_APIKey'), 'sedona');
            $this->getRecords($this->getOption('Copper Country_URL'), $this->getOption('Copper Country_APIID'), $this->getOption('Copper Country_APIKey'), 'copper-country');
        }
        
        // Save Posted Options
        if (isset($_POST["Save"]) && $optionMetaData != null) {
            $this->Sync();
        }

        // HTML for the page
        $settingsGroup = get_class($this) . '-settings-group';
        ?>
        <div class="wrap">
            <h2><?php _e('System Settings', 'mjfreewaysync'); ?></h2>
            <table cellspacing="1" cellpadding="2"><tbody>
            <tr><td><?php _e('System', 'mjfreewaysync'); ?></td><td><?php echo php_uname(); ?></td></tr>
            <tr><td><?php _e('PHP Version', 'mjfreewaysync'); ?></td>
                <td><?php echo phpversion(); ?>
                <?php
                if (version_compare('5.2', phpversion()) > 0) {
                    echo '&nbsp;&nbsp;&nbsp;<span style="background-color: #ffcc00;">';
                    _e('(WARNING: This plugin may not work properly with versions earlier than PHP 5.2)', 'mjfreewaysync');
                    echo '</span>';
                }
                ?>
                </td>
            </tr>
            <tr><td><?php _e('MySQL Version', 'mjfreewaysync'); ?></td>
                <td><?php echo $this->getMySqlVersion() ?>
                    <?php
                    echo '&nbsp;&nbsp;&nbsp;<span style="background-color: #ffcc00;">';
                    if (version_compare('5.0', $this->getMySqlVersion()) > 0) {
                        _e('(WARNING: This plugin may not work properly with versions earlier than MySQL 5.0)', 'mjfreewaysync');
                    }
                    echo '</span>';
                    ?>
                </td>
            </tr>
            </tbody></table>

            <h2><?php echo $this->getPluginDisplayName(); echo ' '; _e('Settings', 'mjfreewaysync'); ?></h2>

            <form method="post" action="">
            <?php settings_fields($settingsGroup); ?>
                <style type="text/css">
                    table.plugin-options-table {width: 100%; padding: 0;}
                    table.plugin-options-table tr:nth-child(even) {background: #f9f9f9}
                    table.plugin-options-table tr:nth-child(odd) {background: #FFF}
                    table.plugin-options-table tr:first-child {width: 35%;}
                    table.plugin-options-table td {vertical-align: middle;}
                    table.plugin-options-table td+td {width: auto}
                    table.plugin-options-table td > p {margin-top: 0; margin-bottom: 0;}
                </style>
                <table class="plugin-options-table"><tbody>
                <?php
                if ($optionMetaData != null) {
                    foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                        $displayText = is_array($aOptionMeta) ? $aOptionMeta[0] : $aOptionMeta;
                        ?>
                            <tr valign="top">
                                <th scope="row"><p><label for="<?php echo $aOptionKey ?>"><?php echo $displayText ?></label></p></th>
                                <td>
                                <?php 
                                    if ($aOptionKey == 'Branch'){
                                        if (isset($_POST['Branch'])){
                                            $this->createFormControl($aOptionKey, $aOptionMeta, $_POST['Branch']);
                                        }
                                        else if (isset($_GET['Branch'])) {
                                            $this->createFormControl($aOptionKey, $aOptionMeta, $_GET['Branch']);
                                        }
                                        else {
                                            $this->createFormControl($aOptionKey, $aOptionMeta, "Phoenix");
                                        }
                                    }
                                    else{
                                        if (isset($_POST['Branch'])){
                                            $this->createFormControl($aOptionKey, $aOptionMeta, $this->getOption($_POST['Branch'] . "_" . $aOptionKey));
                                        }
                                        else if (isset($_GET['Branch'])) {
                                            $this->createFormControl($aOptionKey, $aOptionMeta, $this->getOption($_GET['Branch'] . "_" . $aOptionKey));
                                        }
                                        else {
                                            $this->createFormControl($aOptionKey, $aOptionMeta, $this->getOption("Phoenix_" . $aOptionKey));
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                    }
                }
                
                ?>
                </tbody></table>
                <p class="submit">
                    <input type="submit" class="button-primary" name="Save"
                           value="<?php _e('Save', 'mjfreewaysync') ?>"/>
                </p>
                <p class="submit">
                    <input type="submit" class="button-primary" name="Sync"
                           value="<?php _e('Sync', 'mjfreewaysync') ?>"/>
                </p>
            </form>
        </div>
        <script>
            window.addEventListener('load', function(){
                var branch = document.getElementById("Branch");
                
                if (branch != null){
                    branch.addEventListener('change', function(){
                        window.location = updateQueryStringParameter(
                            window.location.pathname + window.location.search,
                            'Branch', this.value);
                    });
                }
            }, false);
            
            function updateQueryStringParameter(uri, key, value) {
                var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                if (uri.match(re)) {
                    return uri.replace(re, '$1' + key + "=" + value + '$2');
                }
                else {
                    return uri + separator + key + "=" + value;
                }
            }
        </script>
        <?php

    }

    /**
     * Helper-function outputs the correct form element (input tag, select tag) for the given item
     * @param  $aOptionKey string name of the option (un-prefixed)
     * @param  $aOptionMeta mixed meta-data for $aOptionKey (either a string display-name or an array(display-name, option1, option2, ...)
     * @param  $savedOptionValue string current value for $aOptionKey
     * @return void
     */
    protected function createFormControl($aOptionKey, $aOptionMeta, $savedOptionValue) {
        if (is_array($aOptionMeta) && count($aOptionMeta) >= 2) { // Drop-down list
            $choices = array_slice($aOptionMeta, 1);
            ?>
            <p><select name="<?php echo $aOptionKey ?>" id="<?php echo $aOptionKey ?>">
            <?php
                            foreach ($choices as $aChoice) {
                $selected = ($aChoice == $savedOptionValue) ? 'selected' : '';
                ?>
                    <option value="<?php echo $aChoice ?>" <?php echo $selected ?>><?php echo $this->getOptionValueI18nString($aChoice) ?></option>
                <?php
            }
            ?>
            </select></p>
            <?php

        }
        else { // Simple input field
            ?>
            <p><input type="text" name="<?php echo $aOptionKey ?>" id="<?php echo $aOptionKey ?>"
                      value="<?php echo esc_attr($savedOptionValue) ?>" size="50"/></p>
            <?php

        }
    }

    /**
     * Override this method and follow its format.
     * The purpose of this method is to provide i18n display strings for the values of options.
     * For example, you may create a options with values 'true' or 'false'.
     * In the options page, this will show as a drop down list with these choices.
     * But when the the language is not English, you would like to display different strings
     * for 'true' and 'false' while still keeping the value of that option that is actually saved in
     * the DB as 'true' or 'false'.
     * To do this, follow the convention of defining option values in getOptionMetaData() as canonical names
     * (what you want them to literally be, like 'true') and then add each one to the switch statement in this
     * function, returning the "__()" i18n name of that string.
     * @param  $optionValue string
     * @return string __($optionValue) if it is listed in this method, otherwise just returns $optionValue
     */
    protected function getOptionValueI18nString($optionValue) {
        switch ($optionValue) {
            case 'true':
                return __('true', 'mjfreewaysync');
            case 'false':
                return __('false', 'mjfreewaysync');

            case 'Administrator':
                return __('Administrator', 'mjfreewaysync');
            case 'Editor':
                return __('Editor', 'mjfreewaysync');
            case 'Author':
                return __('Author', 'mjfreewaysync');
            case 'Contributor':
                return __('Contributor', 'mjfreewaysync');
            case 'Subscriber':
                return __('Subscriber', 'mjfreewaysync');
            case 'Anyone':
                return __('Anyone', 'mjfreewaysync');
        }
        return $optionValue;
    }

    /**
     * Query MySQL DB for its version
     * @return string|false
     */
    protected function getMySqlVersion() {
        global $wpdb;
        $rows = $wpdb->get_results('select version() as mysqlversion');
        if (!empty($rows)) {
             return $rows[0]->mysqlversion;
        }
        return false;
    }

    /**
     * If you want to generate an email address like "no-reply@your-site.com" then
     * you can use this to get the domain name part.
     * E.g.  'no-reply@' . $this->getEmailDomain();
     * This code was stolen from the wp_mail function, where it generates a default
     * from "wordpress@your-site.com"
     * @return string domain name
     */
    public function getEmailDomain() {
        // Get the site domain and get rid of www.
        $sitename = strtolower($_SERVER['SERVER_NAME']);
        if (substr($sitename, 0, 4) == 'www.') {
            $sitename = substr($sitename, 4);
        }
        return $sitename;
    }
    
    public function getRecords($URL, $APIID, $APIKEY, $branch){
        $recordCount = 0;
        
        try {
            if (empty($URL) || empty($APIID) || empty($APIKEY) || empty($branch)){
                return;
            }
            
            if(is_callable('curl_init')){
                global $wpdb;
                
                $ch = curl_init();  
            
                $postData = 'API_ID=' . $APIID . '&' .
                    'API_KEY=' . $APIKEY . '&' .
                    'version=7&' .
                    'format=JSON'; 
                
                curl_setopt($ch,CURLOPT_URL, $URL);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, count($postData));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
            
                $output = curl_exec($ch);
            
                if($output === false)
                {
                    echo "Error Number:".curl_errno($ch)."<br>";
                    echo "Error String:".curl_error($ch);
                }
                
                curl_close($ch);
                
                $output = json_decode($output);
                
                if ($output->response_header->response_code == 'OK')
                {
                    foreach ($output->response_details->menu_items as $menuItem) {
                        
                        // $post_id = $menuItem->sku; 
                        $post_content = $menuItem->description;
                        $post_title = $menuItem->title;
                        $sku = $menuItem->sku;
                        $post_category = $this->GetProductCategoryID($branch, $menuItem->category);
                        $post_parent_category = $this->GetProductParentCategoryID($branch);
                        $prices = array();
                        // **removed featuredImage b/c images being uploaded manually -PMD**
                        //$featuredImage = $menuItem->product_images[0]->{'full'};
                        $post_type = 'product';
                        $post_status = 'publish';
                        $post_date = $menuItem->last_update;
                        $post_id = -1;
                        $stock = 0;
                        
                        switch($menuItem->current_level->type)
                        {
                            case "prepack":
                                $stock = $menuItem->current_level->level_units;
                                        
                                foreach ($menuItem->price_options as $priceOption) {
                                    $prices[] = array('price' => $priceOption->price, 'description' => $priceOption->description);
                                }
                                
                            break;
                            case "simple":
                                $stock = $menuItem->current_level->level;
                                        
                                foreach ($menuItem->price_options as $priceOption) {
                                    $prices[0] = array('price' => $priceOption, 'description' => 'Each');
                                }
                                
                            break;
                            case "nontracked":
                                $stock = $menuItem->current_level->level;
                                        
                                foreach ($menuItem->price_options as $priceOption) {
                                    $prices[0] = array('price' => $priceOption, 'description' => 'Each');
                                }
                                
                            break;
                            case "bulk":
                                $stock = $menuItem->current_level->level;
                                        
                                foreach ($menuItem->price_options as $priceOption) {
                                    $prices[] = array('price' => $priceOption->price, 'description' => $priceOption->description);
                                }
                                
                            break;
                            default:
                                
                                echo '<p>NEW PRODUCT TYPE ENCOUNTERED</p>';
                                
                                print_r($menuItem);
                                
                                return;
                                
                            break;
                        }
                        
                        $variables = "";
                        
                        foreach ($prices as $price) {
                            if (empty($price['price']) || $price['price'] == 0)
                            {
                                continue;
                            }
                            
                            $variables .= $price['description'] . ' | ';
                        }
                        
                        $variables = rtrim($variables, " | ");
                        
                        $args = array(
                            'post_type' => 'product',
                            'meta_query' => array(
                                array(
                                    'key' => '_sku',
                                    'value' => $sku
                                    )
                                ),
                            'fields' => 'id');
                        
                        $vid_query = new WP_Query( $args );
                        
                        $vid_ids = $vid_query->posts;
                        
                        if(empty($vid_ids)) {
                            $post = array(
                                'import_id'      => $post_id,
                                'post_excerpt'   => $post_content,
                                'post_content'   => $post_content,
                                'post_name'      => $post_title,
                                'post_title'     => $post_title,
                                'post_status'    => $post_status,
                                'post_type'      => $post_type,
                                'post_date'      => $post_date
                                );
                                
                            $post_id = wp_insert_post($post);
                            
                            wp_set_post_terms($post_id, $post_category, 'product_cat');
                            wp_set_post_terms($post_id, $post_parent_category, 'product_cat', true);
                            
                         /*  **removed to stop unloadable images b/c of MJfreeway's shitty cookies -PMD ** 
                         
                             if (!empty($featuredImage)){
                                $thumbnail = array(
                                    'guid'           => str_replace("//i5.", "//order5.", str_replace("\/", "/", $featuredImage)),
                                    'post_name'      => $post_title,
                                    'post_title'     => $post_title,
                                    'post_status'    => 'inherit',
                                    'post_type'      => 'attachment',
                                    'post_date'      => $post_date,
                                    'post_parent'    => 0
                                    );
                                    
                                $thumbnail_id = wp_insert_post($thumbnail);
                                
                                add_post_meta($post_id, '_thumbnail_id', $thumbnail_id);
                            } */
                            
                            $wpdb->query(
                                $wpdb->prepare(
                                " INSERT INTO $wpdb->postmeta " .
                                " (post_id, meta_key, meta_value) " .
                                " VALUES " .
                                " (" . $post_id . ", 'slide_template', 'default'), " .
                                " (" . $post_id . ", 'total_sales', '0'), " .
                                " (" . $post_id . ", 'woocommerce_disable_product_enquiry', 'no'), " .
                                " (" . $post_id . ", '_backorders', 'no'), " .
                                " (" . $post_id . ", '_crosssell_ids', 'a:0:{}'), " .
                                " (" . $post_id . ", '_default_attributes', 'a:0:{}'), " .
                                " (" . $post_id . ", '_downloadable', 'no'), " .
                                " (" . $post_id . ", '_edit_last', '1'), " .
                                // " (" . $post_id . ", '_edit_lock', '1446551885:1'), " .
                                " (" . $post_id . ", '_et_pb_page_layout', 'et_right_sidebar'), " .
                                " (" . $post_id . ", '_et_pb_post_hide_nav', 'off'), " .
                                " (" . $post_id . ", '_et_pb_side_nav', 'off'), " .
                                " (" . $post_id . ", '_featured', 'no'), " .
                                " (" . $post_id . ", '_height', ''), " .
                                " (" . $post_id . ", '_length', ''), " .
                                " (" . $post_id . ", '_manage_stock', 'no'), " .
                                " (" . $post_id . ", '_max_price_variation_id', ''), " .
                                " (" . $post_id . ", '_max_regular_price_variation_id', ''), " .
                                " (" . $post_id . ", '_max_sale_price_variation_id', ''), " .
                                " (" . $post_id . ", '_max_variation_price', ''), " .
                                " (" . $post_id . ", '_max_variation_regular_price', ''), " .
                                " (" . $post_id . ", '_max_variation_sale_price', ''), " .
                                " (" . $post_id . ", '_min_price_variation_id', ''), " .
                                " (" . $post_id . ", '_min_regular_price_variation_id', ''), " .
                                " (" . $post_id . ", '_min_sale_price_variation_id', ''), " .
                                " (" . $post_id . ", '_min_variation_price', ''), " .
                                " (" . $post_id . ", '_min_variation_regular_price', ''), " .
                                " (" . $post_id . ", '_min_variation_sale_price', ''), " .
                                " (" . $post_id . ", '_price', '" . $prices[0]['price'] . "'), " .
                                " (" . $post_id . ", '_product_attributes', 'a:1:{s:5:\"price\";a:6:{s:4:\"name\";s:5:\"Price\";s:5:\"value\";s:" .
                                    strlen($variables) . ":\"" . 
                                    $variables . "\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:0;}}')," .
                                " (" . $post_id . ", '_product_image_gallery', ''), " .
                                " (" . $post_id . ", '_product_version', '2.4.9'), " .
                                " (" . $post_id . ", '_purchase_note', ''), " .
                                " (" . $post_id . ", '_regular_price', '" . $prices[0]['price'] . "'), " .
                                " (" . $post_id . ", '_sale_price', ''), " .
                                " (" . $post_id . ", '_sale_price_dates_from', ''), " .
                                " (" . $post_id . ", '_sale_price_dates_to', ''), " .
                                " (" . $post_id . ", '_sku', '". $sku . "'), " .
                                " (" . $post_id . ", '_sold_individually', ''), " .
                                " (" . $post_id . ", '_stock', '" . $stock . "'), " . 
                                " (" . $post_id . ", '_stock_status', 'instock'), " .
                                " (" . $post_id . ", '_upsell_ids', 'a:0:{}'), " . 
                                " (" . $post_id . ", '_virtual', 'no'), ".
                                " (" . $post_id . ", '_visibility', 'visible'), " .
                                " (" . $post_id . ", '_weight', ''), " .
                                " (" . $post_id . ", '_width', '') "
                                ));
                                
                            $recordCount++;
                        }
                        else
                        {
                            $post_id = $vid_ids[0]->ID;
                            
                            $post = array(
                                'ID'             => $post_id,
                                'post_excerpt'   => $post_content,
                                'post_content'   => $post_content,
                                'post_name'      => $post_title,
                                'post_title'     => $post_title,
                                'post_status'    => $post_status,
                                'post_type'      => $post_type,
                                'post_date'      => $post_date
                                );
                                
                            $post_id = wp_update_post($post);

                            //wp_remove_object_terms($post_id, wp_get_object_terms($post_id, 'product_cat', array('fields' => 'ids')), 'product_cat');
                            //
                            //wp_set_post_terms($post_id, $post_category, 'product_cat');
                            //wp_set_post_terms($post_id, $post_parent_category, 'product_cat', true);
 
                            $wpdb->query(
                                $wpdb->prepare(
                                " UPDATE $wpdb->postmeta " .
                                " SET meta_value = '2.4.9' " .
                                "   WHERE post_id = " . $post_id . " AND meta_key = '_product_version'"));
                        
                            $wpdb->query(
                                $wpdb->prepare(
                                " UPDATE $wpdb->postmeta " .
                                " SET meta_value = '' " .
                                "   WHERE post_id = " . $post_id . " AND meta_key = '_regular_price'"));
                        
                            if (!empty($prices))
                            {
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = 'a:1:{s:5:\"price\";a:6:{s:4:\"name\";s:5:\"Price\";s:5:\"value\";s:" . 
                                        strlen($variables) . ":\"" . 
                                        $variables . "\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:0;}}' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_product_attributes'"));
                            }
                            
                            if (!empty($stock))
                            {   
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $stock . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_stock'"));
                            }
                               
                            $recordCount++;
                        }
                                        
                        $wpdb->query(
                            $wpdb->prepare(
                                "DELETE FROM $wpdb->postmeta WHERE post_id in (SELECT ID FROM $wpdb->posts WHERE post_type = 'product_variation' AND post_parent = " . $post_id . ")"));
                        
                        $wpdb->query(
                            $wpdb->prepare(
                                "DELETE FROM $wpdb->posts WHERE post_type = 'product_variation' AND post_parent = '" . $post_id . "'"));
                        
                        if (count($prices) > 1)
                        {
                            $minPrice = -1;
                            $maxPrice = -1;
                            $minPriceID = -1;
                            $maxPriceID = -1;
                        
                            wp_set_object_terms($post_id, 'variable', 'product_type');
                            
                            for($i = 0; $i <= count($prices); $i++)
                            {
                                if (empty($prices[$i]['description']))
                                {
                                    continue;
                                }
                                
                                $post = array(
                                    'post_parent'    => $post_id,
                                    'post_excerpt'   => '',
                                    'post_content'   => '',
                                    'post_name'      => 'product-' . $post_id . '-variation-' . $i,
                                    'post_title'     => 'Variation #' . $i . ' of ' . $post_title . ' SKU ' . $sku,
                                    'post_status'    => $post_status,
                                    'post_type'      => 'product_variation'
                                    );
                                    
                                $current_post_id = wp_insert_post($post);
                            
                                if (!empty($prices[$i]['price']) && ($minPrice == -1 || $minPrice > $prices[$i]['price']))
                                {
                                    $minPrice = $prices[$i]['price'];
                                    $minPriceID = $current_post_id;
                                }
                                
                                if (!empty($prices[$i]['price']) && ($maxPrice == -1 || $maxPrice < $prices[$i]['price']))
                                {
                                    $maxPrice = $prices[$i]['price'];
                                    $maxPriceID = $current_post_id;
                                }
                                
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " INSERT INTO $wpdb->postmeta " .
                                    " (post_id, meta_key, meta_value) " .
                                    " VALUES " .
                                    " (" . $current_post_id . ", '_backorders', 'no'), " .
                                    " (" . $current_post_id . ", '_downloadable', 'no'), " .
                                    " (" . $current_post_id . ", '_downloadable_files', ''), " .
                                    " (" . $current_post_id . ", '_download_expiry', ''), " .
                                    " (" . $current_post_id . ", '_download_limit', ''), " .
                                    " (" . $current_post_id . ", '_height', ''), " .
                                    " (" . $current_post_id . ", '_length', ''), " .
                                    " (" . $current_post_id . ", '_manage_stock', 'yes'), " .
                                    " (" . $current_post_id . ", '_price', '" . $prices[$i]['price'] . "'), " .
                                    " (" . $current_post_id . ", '_regular_price', '" . $prices[$i]['price'] . "'), " .
                                    " (" . $current_post_id . ", '_sale_price', ''), " .
                                    " (" . $current_post_id . ", '_sale_price_dates_from', ''), " .
                                    " (" . $current_post_id . ", '_sale_price_dates_to', ''), " .
                                    " (" . $current_post_id . ", '_sku', ''), " .
                                    " (" . $current_post_id . ", '_stock', '" . $stock . "'), " .
                                    " (" . $current_post_id . ", '_stock_status', 'instock'), " .
                                    " (" . $current_post_id . ", '_thumbnail_id', '0'), " .
                                    " (" . $current_post_id . ", '_variation_description', ''), " .
                                    " (" . $current_post_id . ", '_virtual', 'no'), " .
                                    " (" . $current_post_id . ", '_weight', ''), " .
                                    " (" . $current_post_id . ", '_width', ''), " .
                                    " (" . $current_post_id . ", 'attribute_price', '" . $prices[$i]['description'] . "') "
                                    ));
                            }
                            
                            if ($minPriceID != -1)
                            {
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $minPriceID . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_min_price_variation_id'"));
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $minPriceID . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_min_regular_price_variation_id'"));
                                    
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $minPrice . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_min_variation_price'"));
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $minPrice . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_min_variation_regular_price'"));
                            }
                            
                            if ($maxPriceID != -1)
                            {
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $maxPriceID . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_max_price_variation_id'"));
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $maxPriceID . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_max_regular_price_variation_id'"));
                                    
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $maxPrice . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_max_variation_price'"));
                                $wpdb->query(
                                    $wpdb->prepare(
                                    " UPDATE $wpdb->postmeta " .
                                    " SET meta_value = '" . $maxPrice . "' " .
                                    "   WHERE post_id = " . $post_id . " AND meta_key = '_max_variation_regular_price'"));
                            }
                        }
                        else
                        {
                            wp_set_object_terms($post_id, 'simple', 'product_type');
                        }
                    }
                }
                else
                {
                    echo '<h2>Error occurred during sync</h2>';
                }
            }
            else {
                print_r('CURL is not installed.');
            }
        }
        catch (Exception $e){
            print_r($e);
        }
        
        print_r('<p>' . $branch . ' - ' . $recordCount . ' record(s) imported</p>');
    }
    
    function GetProductParentCategoryID($branch){
        return get_term_by('slug', $branch, 'product_cat')->term_id;
    }
    
    function GetProductCategoryID($branch, $categoryName){
        $id = -1;
        
        switch ($categoryName) {
            case 'Medicine': // medicine
            case 'Medicine ':
            case 'Shake ':
            case 'Shake':
            case 'THC Tinctures':
                $id = get_term_by('slug', $branch . '-medicine', 'product_cat')->term_id;
                break;
            case 'Medicine-Prerolls': // medicine pre-rolls
                $id = get_term_by('slug', $branch . '-medicine-pre-rolls', 'product_cat')->term_id;
                break;
            case 'CBD ': // cbd
            case 'CBD':
                $id = get_term_by('slug', $branch . '-cbd', 'product_cat')->term_id;
                break;
            case 'Concentrates ': // concentrates
            case 'Concentrates':
                $id = get_term_by('slug', $branch . '-concentrates', 'product_cat')->term_id;
                break;
            case 'Edibles ': // edibles
            case 'Edibles':
                $id = get_term_by('slug', $branch . '-edibles', 'product_cat')->term_id;
                break;
            case 'Vape Kits \\u0026 Pens': // vape, cartridges and pens
            case 'Vape Kits & Pens':
            case 'Vape Kits and Pens':
            case 'Vape Cartridges':
            case 'dos Kits \\u0026 Pens':
            case 'dos Kits & Pens':
            case 'dos Kits and Pens':
            case 'dos Vape Cartridges \\u0026 Pens':
            case 'dos Vape Cartridges & Pens':
            case 'dos Vape Cartridges and Pens':
            case 'dos Disposable Pens ':
            case 'Dream Steam ':
            case 'Dream Steam':
                $id = get_term_by('slug', $branch . '-vape-cartridges-and-pens', 'product_cat')->term_id;
                break;
            default:
                $id = get_term_by('slug', $branch . '-other', 'product_cat')->term_id;
                break;
        }
        
        return array($id);
    }
    
    function DeleteRecords(){
        global $wpdb;
        
        $wpdb->query(
            $wpdb->prepare(
                "DELETE FROM $wpdb->posts WHERE post_type = 'attachment' AND guid LIKE '%gomjfreeway%'"));
        
        $wpdb->query(
            $wpdb->prepare(
                "DELETE FROM $wpdb->postmeta WHERE post_id in (SELECT ID FROM wp_thyyy39yn2_posts WHERE post_type = 'product')"));
        
        $wpdb->query(
            $wpdb->prepare(
                "DELETE FROM $wpdb->posts WHERE post_type = 'product'"));
    }
    
}

?>